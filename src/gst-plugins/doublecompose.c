#include <config.h>
#include <gst/gst.h>

#include "gstdoublecompose.h"

static gboolean
init(GstPlugin *plugin)
{
  if (!gst_double_compose_plugin_init(plugin))
    return FALSE;

  return TRUE;
}

GST_PLUGIN_DEFINE(GST_VERSION_MAJOR, GST_VERSION_MINOR,
                  doublecompose,
                  "Filter documentation",
                  init, VERSION, GST_LICENSE_UNKNOWN, "DoubleCompose", "origin")
