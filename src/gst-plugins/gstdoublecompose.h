#ifndef _GST_DOUBLE_COMPOSE_H_
#define _GST_DOUBLE_COMPOSE_H_

#include <commons/kmsbasehub.h>

#define AUDIO_SINK_PAD_PREFIX "sink_"
#define AUDIO_SRC_PAD_PREFIX "src_"
#define LENGTH_AUDIO_SINK_PAD_PREFIX (sizeof(AUDIO_SINK_PAD_PREFIX) - 1)
#define LENGTH_AUDIO_SRC_PAD_PREFIX (sizeof(AUDIO_SRC_PAD_PREFIX) - 1)
#define AUDIO_SINK_PAD AUDIO_SINK_PAD_PREFIX "%u"
#define AUDIO_SRC_PAD AUDIO_SRC_PAD_PREFIX "%u"

G_BEGIN_DECLS

#define GST_TYPE_DOUBLE_COMPOSE (gst_double_compose_get_type())
#define GST_DOUBLE_COMPOSE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_DOUBLE_COMPOSE, GstDoubleCompose))
#define GST_DOUBLE_COMPOSE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_DOUBLE_COMPOSE, GstDoubleComposeClass))
#define GST_IS_DOUBLE_COMPOSE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_DOUBLE_COMPOSE))
#define GST_IS_DOUBLE_COMPOSE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_DOUBLE_COMPOSE))
typedef struct _GstDoubleCompose GstDoubleCompose;
typedef struct _GstDoubleComposeClass GstDoubleComposeClass;
typedef struct _GstDoubleComposePrivate GstDoubleComposePrivate;

struct _GstDoubleCompose
{
  KmsBaseHub parent;
  GstDoubleComposePrivate *priv;
};

struct _GstDoubleComposeClass
{
  KmsBaseHubClass parent_class;
};

GType gst_double_compose_get_type(void);

gboolean gst_double_compose_plugin_init(GstPlugin *plugin);

G_END_DECLS

#endif /* _GST_DOUBLE_COMPOSE_H_ */
